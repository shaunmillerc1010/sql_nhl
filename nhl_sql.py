import sqlite3
import pandas as pd

''' A simple example of pandas and sqlite to print the 2016-2017 members of Detriot Redwings from
the data set: https://theleafsnation.com/2017/09/08/an-introduction-to-sql-using-hockey-stats/
the file is saved as 1617_stats_allskaters.csv
First we use pandas to convert the csv to a SQL table then display the table'''


df = pd.read_csv('1617_stats_allskaters.csv')
con = sqlite3.connect(":memory:")
df.to_sql('skaters', con=con,if_exists='append', index=False)
t = ('DET',)
for r in con.execute('SELECT * FROM skaters WHERE Team=?', t): print(r)
